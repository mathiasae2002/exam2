using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class White : MonoBehaviour
{
    public IWhiteStrategy attack;
    public float health;
    public Transform shoot;
    public Transform down;
    public GameObject bullet;
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        attack = new WhiteAttack(this,down,2f);
    }

    // Update is called once per frame
    void Update()
    {
        attack.Execute();
        timer += Time.deltaTime;
        if(timer >= 3f)
        {
            Instantiate(bullet,shoot.position,Quaternion.identity);
            timer =0f;
        }
        if(health<= 0)
        {
            Destroy(this.gameObject);
        }
    }
    
}
