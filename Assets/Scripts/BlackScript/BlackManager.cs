using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackManager : MonoBehaviour, ISubjectBlack
{
    // Start is called before the first frame update
    private float timer;
    private List<IObserverBlack> observers = new List<IObserverBlack>();

    public void Attach(IObserverBlack observer)
    {
        observers.Add(observer);
    }
    public void Detach(IObserverBlack observer)
    {
        observers.Remove(observer);
    }
    // Update is called once per frame
    public void Notify()
    {
        foreach(IObserverBlack observer in observers){
            observer.Execute(this);
        }
    }
    private void Update() {
        timer += Time.deltaTime;
        if(timer>= 4f){
            Notify();
            timer =0f;
        }
    }
}
