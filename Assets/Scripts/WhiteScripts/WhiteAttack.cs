using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteAttack : IWhiteStrategy
{   
    private White white;
    private Transform down;
    
    private float speed;

    public WhiteAttack(White white, Transform down, float speed){
        this.white = white;
        this.down = down;
        this.speed = speed;
    }
    public void Execute()
    {
        
        white.transform.position = Vector3.MoveTowards(white.transform.position, down.position, speed*Time.deltaTime);
    }
}
