using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private static PlayerManager instance;
    public Rigidbody rb;
    public Transform orientation;
    public Transform shoot;
    public GameObject bulletb;
    public GameObject bulletw;
    private Vector3 movedirection;
    public float health;
    public float speed;
    // Start is called before the first frame update
    private void Awake()
    {
        if(instance ==null){
            instance=this;
        }
        else{
            Destroy(this.gameObject);
        }
    }
    void Start()
    {
        rb=GetComponent<Rigidbody>();
    }

    public void Movement()
    {
        float horizontalInput =Input.GetAxisRaw("Horizontal");
        float verticalInput=Input.GetAxisRaw("Vertical");
        movedirection= orientation.forward *verticalInput + orientation.right*horizontalInput;

        rb.AddForce( movedirection*speed*10f,ForceMode.Force);
    }
    // Update is called once per frame
    void Update()
    {
        Movement();

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Instantiate(bulletb,shoot.position,Quaternion.identity);
        }
        else if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            Instantiate(bulletw,shoot.position,Quaternion.identity);
        }
    }
}
