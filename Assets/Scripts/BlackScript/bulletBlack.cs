using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletBlack : MonoBehaviour
{
    private PlayerManager life;
    private Transform Player;
    public float timer=0f;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position,Player.position,2f*Time.deltaTime);
        timer +=Time.deltaTime;
        if(timer>=2f){
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag=="Player")
        {   
            life = other.GetComponent<PlayerManager>();
            life.health -= 10f;
            Destroy(this.gameObject);
        } 
    }
}
