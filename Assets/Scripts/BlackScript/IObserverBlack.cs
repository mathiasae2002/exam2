using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObserverBlack
{
    public void Execute(ISubjectBlack subject);
}
