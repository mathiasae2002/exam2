using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletW : MonoBehaviour
{
    private PlayerManager life;
    public float timer=0f;
    public Transform point;

    // Start is called before the first frame update
    void Start()
    {
        point = GameObject.Find("Point1").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position,point.position,2f*Time.deltaTime);
        timer +=Time.deltaTime;
        if(timer>=2f){
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag=="Player")
        {   
            life = other.GetComponent<PlayerManager>();
            life.health -= 10f;
            Destroy(this.gameObject);
        } 
    }
        
}

