using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISubjectBlack
{
    void Attach(IObserverBlack observer);
    void Detach(IObserverBlack observer);
    void Notify();
}
