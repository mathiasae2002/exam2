using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletWPlayer : MonoBehaviour
{
    private White life;
    public float timer=0f;
    private Transform point;

    // Start is called before the first frame update
    void Start()
    {
        point = GameObject.Find("PointP").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position,point.position,2f*Time.deltaTime);
        timer +=Time.deltaTime;
        if(timer>=2f){
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag=="EnemyW")
        {   
            life = other.GetComponent<White>();
            life.health -= 10f;
            Destroy(this.gameObject);
        } 
    }
}
