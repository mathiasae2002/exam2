using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackAttack : MonoBehaviour, IObserverBlack
{
    public float speed;
    public float health;
    public GameObject bullet;
    public Transform down;
    public Transform shoot;
    void Start()
    {
        GameObject.Find("BlackManager").GetComponent<BlackManager>().Attach(this);
    }

    public void Execute(ISubjectBlack subject)
    {
        if(subject is BlackManager)
        {
            Instantiate(bullet,shoot.position,Quaternion.identity);
        }
    }
    void Update() {
        transform.position= Vector3.MoveTowards(transform.position,down.position,speed*Time.deltaTime);
        if(health<=0){
            Destroy(this.gameObject);
        }
    }
}
