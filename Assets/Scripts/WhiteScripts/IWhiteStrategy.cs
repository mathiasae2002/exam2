using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWhiteStrategy
{
    public void Execute();
}
